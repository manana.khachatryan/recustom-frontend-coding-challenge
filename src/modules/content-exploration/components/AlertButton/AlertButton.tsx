"use client";

import {
  Button as MuiButton,
  ButtonProps as MuiButtonProps,
} from "@mui/material";
import { styled } from "@mui/material/styles";

export type ButtonProps = MuiButtonProps & {};

const AlertButton = styled(MuiButton, {
  name: "Button",
  slot: "root",
})<ButtonProps>(({ color = "success" }) => ({
  borderRadius: 100,
  border: "1px solid",
  textTransform: "initial",
  borderColor: "#000",
  color: "#fff",
  display: "block",
  lineHeight: "10px",
  marginTop: 10,
  height: 26,
  width: 91,
  fontSize: 12,
  backgroundColor: "#00AC80",
  ...(color === "error" && {
    backgroundColor: "#FF6464",
  }),
  ...(color === "warning" && {
    backgroundColor: "#FFFF00",
    color: "#000",
  }),
}));

export default AlertButton;
