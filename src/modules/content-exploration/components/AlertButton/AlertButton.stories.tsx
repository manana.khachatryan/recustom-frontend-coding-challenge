import React from "react";
import { Meta, Story } from "@storybook/react";
import AlertButton, { ButtonProps } from "./AlertButton";

export default {
  title: "Components/AlertButton",
  component: AlertButton,
} as Meta;

const Template: Story<ButtonProps> = (args) => <AlertButton {...args} />;

export const Default = Template.bind({});
Default.args = {
  color: "success",
  children: "Default",
};

export const WarningButton = Template.bind({});
WarningButton.args = {
  color: "warning",
  children: "Warning",
};

export const ErrorButton = Template.bind({});
ErrorButton.args = {
  color: "error",
  children: "Error",
};
