import React from "react";
import { render } from "@testing-library/react";
import AlertButton from "../AlertButton";

describe("AlertButton Component", () => {
  it("renders with default props", () => {
    const { getByTestId } = render(<AlertButton data-testid="button" />);
    const buttonRoot = getByTestId("button");

    // Assert on default styles
    expect(buttonRoot).toHaveStyle("border-radius: 100px");
    expect(buttonRoot).toHaveStyle("border: 1px solid");
  });

  it("renders with error color", () => {
    const { getByTestId } = render(
      <AlertButton color="error" data-testid="button" />
    );
    const buttonRoot = getByTestId("button");

    // Assert on styles specific to error color
    expect(buttonRoot).toHaveStyle("background-color: rgba(211, 47, 47, 0.04)");
  });

  it("renders with warning color", () => {
    const { getByTestId } = render(
      <AlertButton color="warning" data-testid="button" />
    );
    const buttonRoot = getByTestId("button");

    // Assert on styles specific to warning color
    expect(buttonRoot).toHaveStyle("background-color: rgba(237, 108, 2, 0.04)");
    expect(buttonRoot).toHaveStyle("color: #000");
  });
});
