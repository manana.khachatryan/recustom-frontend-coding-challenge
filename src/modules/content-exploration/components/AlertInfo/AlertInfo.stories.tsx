import React from "react";
import { Meta, Story } from "@storybook/react";
import { IconButton, AlertTitle } from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import AlertInfo, { AlertProps } from "./AlertInfo";
import CheckIcon from "@mui/icons-material/Check";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import WarningIcon from "@mui/icons-material/Warning";

export default {
  title: "Components/AlertInfo",
  component: AlertInfo,
} as Meta;

const Template: Story<AlertProps> = (args) => <AlertInfo {...args} />;

export const Default = Template.bind({});
Default.args = {
  severity: "success",
  icon: (
    <div>
      <CheckIcon fontSize="inherit" />
    </div>
  ),
  children: "The action that you have done was a success! Well done",
  action: (
    <IconButton aria-label="close" color="inherit" size="small">
      <CloseIcon fontSize="inherit" />
    </IconButton>
  ),
};

export const SuccessAlertInfo = Template.bind({});
SuccessAlertInfo.args = {
  severity: "success",
  icon: false,
  children: (
    <>
      <div>
        <CheckCircleIcon fontSize="small" />
        <AlertTitle>Success</AlertTitle>
      </div>
      <div>
        Well done, you successfully read this important alert message. This
        example text is going to run a bit longer so that you can see how
        spacing within an alert works with this kind of content. Be sure to use
        margin utilities to keep things nice and tidy.
      </div>
    </>
  ),
  action: (
    <IconButton aria-label="close" color="inherit" size="small">
      <CloseIcon fontSize="inherit" />
    </IconButton>
  ),
};

export const ErrorAlertInfo = Template.bind({});
ErrorAlertInfo.args = {
  severity: "error",
  icon: false,
  children: (
    <>
      <div>
        <WarningIcon fontSize="small" />
        <AlertTitle>Attention</AlertTitle>
      </div>
      Oh snap, you successfully read this important alert message. This example
      text is going to run a bit longer so that you can see how spacing within
      an alert works with this kind of content. Be sure to use margin utilities
      to keep things nice and tidy.
    </>
  ),
  action: (
    <IconButton aria-label="close" color="inherit" size="small">
      <CloseIcon fontSize="inherit" />
    </IconButton>
  ),
};

export const ErrorAlert = Template.bind({});
ErrorAlert.args = {
  severity: "error",
  icon: (
    <div>
      <NotificationsNoneIcon fontSize="inherit" />
    </div>
  ),

  action: (
    <IconButton aria-label="close" color="inherit" size="small">
      <CloseIcon fontSize="inherit" />
    </IconButton>
  ),
  children: "The action that you have done was a success! Well done",
};
