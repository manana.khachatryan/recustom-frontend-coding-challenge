"use client";

import { Alert as MuiAlert, AlertProps as MuiAlertProps } from "@mui/material";
import { styled } from "@mui/material/styles";

export type AlertProps = MuiAlertProps & {};

const AlertInfo = styled(MuiAlert, {
  name: "Alert",
  slot: "root",
})<AlertProps>(
  ({ theme, color = "default", size = "long", severity = "default" }) => ({
    borderRadius: 6,
    borderColor: "#F3F4F6",
    color: "#6B7280",
    padding: "9px 16px",
    width: 640,
    ".MuiAlert-icon div": {
      width: 32,
      height: 32,
      padding: 0,
      display: "flex",
      alignItems: "center",
      justifyContent: "center",
      borderRadius: 7,
      backgroundColor: "#CCF7EC",
    },
    ".MuiAlert-icon": {
      padding: 0,
    },
    "& svg": {
      color: "#9CA3AF",
    },
    ...(size === "small" && {
      width: 375,
    }),
    ".MuiAlert-message > div": {
      display: "flex",
      justifyContent: "start",
      alignItems: "center",
      marginBottom: 10,
      ".MuiAlertTitle-root": {
        padding: "0 0 0 10px",
        margin: 0,
      },
    },

    ".MuiAlert-message": {
      padding: "4px 0 3px",
    },
    ...(severity === "success" && {
      borderColor: "#66e7c6",
      color: "#00ac80",
      ".MuiAlert-icon div": {
        width: 32,
        height: 32,
        padding: 0,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 7,
        backgroundColor: "#CCF7EC",
      },
      "& svg": {
        color: "#00AC80",
      },
    }),
    ...(severity === "error" && {
      borderColor: "#FFA2A2",
      color: "#FF6464",
      ".MuiAlert-icon div": {
        width: 32,
        height: 32,
        padding: 0,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 7,
        backgroundColor: "#FFE0E0",
      },
      "& svg": {
        color: "#FF6464",
      },
    }),
  })
);

export default AlertInfo;
