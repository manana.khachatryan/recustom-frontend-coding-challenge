import React from "react";
import { render } from "@testing-library/react";
import AlertInfo from "../AlertInfo";

describe("AlertInfo Component", () => {
  it("renders with default props", () => {
    const { getByTestId } = render(<AlertInfo data-testid="alert" />);
    const alertRoot = getByTestId("alert");

    expect(alertRoot).toHaveStyle("borderRadius: 6px");
    expect(alertRoot).toHaveStyle("borderColor: #F3F4F6");
  });

  it("renders with success severity", () => {
    const { getByTestId } = render(
      <AlertInfo severity="success" data-testid="alert" />
    );
    const alertRoot = getByTestId("alert");

    expect(alertRoot).toHaveStyle("borderColor: #66e7c6");
    expect(alertRoot).toHaveStyle("color: #00ac80");
  });

  it("renders with error severity", () => {
    const { getByTestId } = render(
      <AlertInfo severity="error" data-testid="alert" />
    );
    const alertRoot = getByTestId("alert");

    expect(alertRoot).toHaveStyle("borderColor: #FFA2A2");
    expect(alertRoot).toHaveStyle("color: #FF6464");
  });
});
