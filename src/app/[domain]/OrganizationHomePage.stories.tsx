import type { Meta, StoryObj } from "@storybook/react";

import OrganizationHomePage from "./OrganizationHomePage";

//👇 This default export determines where your story goes in the story list
const meta: Meta<typeof OrganizationHomePage> = {
  component: OrganizationHomePage,
};

export default meta;
type Story = StoryObj<typeof OrganizationHomePage>;

export const FirstStory: Story = {
  args: {
    //👇 The args you need here will depend on your component
  },
};
