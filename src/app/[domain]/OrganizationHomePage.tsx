import ChannelList from "@/modules/content-exploration/components/ChannelList/ChannelList";
import {
  Alert,
  Container,
  Stack,
  Button,
  Typography,
  IconButton,
  AlertTitle,
} from "@mui/material";
import CloseIcon from "@mui/icons-material/Close";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";
import AlertInfo from "@/modules/content-exploration/components/AlertInfo/AlertInfo";
import OrgHero from "@/modules/content-exploration/components/OrgHero/OrgHero";
import CheckIcon from "@mui/icons-material/Check";
import AlertButton from "@/modules/content-exploration/components/AlertButton/AlertButton";
import WarningIcon from "@mui/icons-material/Warning";
import NotificationsNoneIcon from "@mui/icons-material/NotificationsNone";
import AccountCircleIcon from "@mui/icons-material/AccountCircle";

export default function OrganizationHomePage({
  params,
}: {
  params: {
    domain: string;
  };
}) {
  return (
    <Container component="main">
      <Stack spacing={2}>
        <OrgHero />
        <ChannelList />

        <Typography variant="h2" textAlign="center">
          Buttons
        </Typography>
        <Stack direction="row" spacing={2} justifyContent="center">
          <Button variant="contained">Default Button</Button>
          <Button variant="contained" color="secondary">
            Secondary button
          </Button>
        </Stack>

        <Typography variant="h2" textAlign="center">
          Alerts
        </Typography>
        <Alert severity="error">This is an error alert — check it out!</Alert>
        <Alert severity="warning">
          This is a warning alert — check it out!
        </Alert>
        <Alert severity="info">This is an info alert — check it out!</Alert>
        <Alert severity="success">
          This is a success alert — check it out!
        </Alert>
        <Typography variant="h2" textAlign="center">
          Toasts
        </Typography>
        <Stack
          direction="row"
          spacing={3}
          justifyContent="start"
          alignItems="flex-start"
        >
          <AlertInfo
            icon={
              <div>
                <CheckIcon fontSize="inherit" />
              </div>
            }
            variant="outlined"
            severity="success"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            The action that you have done was a success! Well done
          </AlertInfo>
          <AlertInfo
            icon={
              <div>
                <CheckIcon fontSize="inherit" />
              </div>
            }
            variant="outlined"
            severity="success"
            size="small"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            The action that you have done was a success! Well done
          </AlertInfo>
        </Stack>
        <Stack
          direction="row"
          spacing={3}
          justifyContent="start"
          alignItems="flex-start"
        >
          <AlertInfo
            icon={false}
            variant="outlined"
            severity="success"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            CheckCircleIconCheckCircleIcon
          </AlertInfo>
          <AlertInfo
            icon={false}
            variant="outlined"
            severity="success"
            size="small"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            <div>
              <CheckCircleIcon fontSize="small" />
              <AlertTitle>Success</AlertTitle>
            </div>
            Well done, you successfully read this important alert message. This
            example text is going to run a bit longer so that you can see how
            spacing within an alert works with this kind of content. Be sure to
            use margin utilities to keep things nice and tidy.
            <AlertButton color="success">Take action</AlertButton>
          </AlertInfo>
        </Stack>
        <Stack
          direction="row"
          spacing={3}
          justifyContent="start"
          alignItems="flex-start"
        >
          <AlertInfo
            icon={
              <div>
                <NotificationsNoneIcon fontSize="inherit" />
              </div>
            }
            variant="outlined"
            severity="error"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            The action that you have done was a success! Well done
          </AlertInfo>
          <AlertInfo
            icon={
              <div>
                <NotificationsNoneIcon fontSize="inherit" />
              </div>
            }
            variant="outlined"
            severity="error"
            size="small"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            The action that you have done was a success! Well done
          </AlertInfo>
        </Stack>
        <Stack
          direction="row"
          spacing={3}
          justifyContent="start"
          alignItems="flex-start"
        >
          <AlertInfo
            icon={false}
            variant="outlined"
            severity="error"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            <div>
              <WarningIcon fontSize="small" />
              <AlertTitle>Attention</AlertTitle>
            </div>
            Oh snap, you successfully read this important alert message. This
            example text is going to run a bit longer so that you can see how
            spacing within an alert works with this kind of content. Be sure to
            use margin utilities to keep things nice and tidy.
            <AlertButton color="error">Take action</AlertButton>
          </AlertInfo>
          <AlertInfo
            icon={false}
            variant="outlined"
            severity="error"
            size="small"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            <div>
              <WarningIcon fontSize="small" />
              <AlertTitle>Attention</AlertTitle>
            </div>
            Oh snap, you successfully read this important alert message. This
            example text is going to run a bit longer so that you can see how
            spacing within an alert works with this kind of content. Be sure to
            use margin utilities to keep things nice and tidy.
            <AlertButton color="error">Take action</AlertButton>
          </AlertInfo>
        </Stack>
        <Stack
          direction="row"
          spacing={3}
          justifyContent="start"
          alignItems="flex-start"
        >
          <AlertInfo
            icon={<AccountCircleIcon fontSize="large" />}
            variant="outlined"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            <AlertTitle>Bonnie Green</AlertTitle>
            Hi Neil, thanks for sharing your thoughts regardingFlowbite.
            <AlertButton color="warning">Button text</AlertButton>
          </AlertInfo>
          <AlertInfo
            icon={<AccountCircleIcon fontSize="large" />}
            variant="outlined"
            size="small"
            action={
              <IconButton aria-label="close" color="inherit" size="small">
                <CloseIcon fontSize="inherit" />
              </IconButton>
            }
          >
            <AlertTitle>Bonnie Green</AlertTitle>
            Hi Neil, thanks for sharing your thoughts regardingFlowbite.
            <AlertButton color="warning">Button text</AlertButton>
          </AlertInfo>
        </Stack>
      </Stack>
    </Container>
  );
}
